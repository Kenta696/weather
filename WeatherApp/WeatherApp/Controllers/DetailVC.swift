//
//  DetailVC.swift
//  WeatherApp
//
//  Created by Raman Krutsiou on 5/13/21.
//

import UIKit
import GoogleMaps

class DetailVC: UIViewController {
    
    var index:Int?
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var skyStateLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var feelTempLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var windLabel: UILabel!
    @IBOutlet weak var windDirectLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var contentStack: UIStackView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var exit: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.layer.cornerRadius = 10
        dateLabel.text = "Request from \(DataSource.shared.data[index!].createDate)"
        setStackLabel()
        setMapParametr()
        setButton()
    }
    

    func setMapParametr() {
        let marker = GMSMarker(position: CLLocationCoordinate2D(latitude:DataSource.shared.data[index!].lat, longitude:DataSource.shared.data[index!].lon))
        marker.title = "\(DataSource.shared.data[index!].place),\(DataSource.shared.data[index!].country)"
        marker.map = mapView
        
        let camera = GMSCameraPosition(latitude:DataSource.shared.data[index!].lat, longitude: DataSource.shared.data[index!].lon, zoom: 15)
        mapView.camera = camera
        
    }
    
    
    func setButton()  {
        exit.layer.cornerRadius = exit.bounds.height / 2
        exit.backgroundColor = UIColor.init(crayola:"Maroon")
        exit.setTitleColor(UIColor.black, for:.normal)
    }
    
    func setStackLabel() {
        skyStateLabel.text = "Sky: \(DataSource.shared.data[index!].skyState)"
        if DataSource.shared.data[index!].temperature >= 0 {
            tempLabel.text = "Temperature: +\(DataSource.shared.data[index!].temperature)"
        }else{
            tempLabel.text = "Temperature: -\(DataSource.shared.data[index!].temperature)"
        }
        if DataSource.shared.data[index!].feelsTemperature >= 0 {
            tempLabel.text = "Feels like: +\(DataSource.shared.data[index!].feelsTemperature)"
        }else{
            tempLabel.text = "Feels like: -\(DataSource.shared.data[index!].feelsTemperature)"
        }
        pressureLabel.text = "Pressure:\(DataSource.shared.data[index!].pressure) nPA"
        humidityLabel.text = "Humidty:\(DataSource.shared.data[index!].humidity)%"
        
        windLabel.text = "Wind speep: \(DataSource.shared.data[index!].windSpeed) m/s"
        
        switch DataSource.shared.data[index!].windDirection {
        case 0...30:
            windDirectLabel.text = "Wind direction: North Wind"
        case 31...60:
            windDirectLabel.text = "Wind direction: North-East Wind"
        case 61...120:
            windDirectLabel.text = "Wind direction: East Wind"
        case 121...150:
            windDirectLabel.text = "Wind direction: South-East Wind"
        case 151...210:
            windDirectLabel.text = "Wind direction: South Wind"
        case 211...240:
            windDirectLabel.text = "Wind direction: South-West Wind"
        case 241...300:
            windDirectLabel.text = "Wind direction: West Wind"
        case 301...330:
            windDirectLabel.text = "Wind direction: North-West Wind"
        case 331...359:
            windDirectLabel.text = "Wind direction: North Wind"
        default:
            break
        }
        countryLabel.text = "\(DataSource.shared.data[index!].place),\(DataSource.shared.data[index!].country)"
    }
    
    
    @IBAction func exitAction(_ sender: Any) {
        
        navigationController?.popToRootViewController(animated: true)
        
    }
    
}

//
//  Wikipedia.swift
//  WeatherApp
//
//  Created by Raman Krutsiou on 5/20/21.
//

import UIKit
import SafariServices

class Wikipedia: UIViewController {
    
    var place:String?
    var urlWiki = "https://en.wikipedia.org/wiki/"

    override func viewDidLoad() {
        super.viewDidLoad()

        let url = urlWiki + (place ?? "Dortmund")
        
        guard let adres = URL(string: url) else { return  }
        
        let brows = SFSafariViewController(url: adres)
        
        modalPresentationStyle = .fullScreen
        present(brows, animated: true)
        
        
    }
    
}

//
//  MapVC.swift
//  WeatherApp
//
//  Created by Raman Krutsiou on 5/7/21.
//

import UIKit
import GoogleMaps
import EDColor

class MapVC: UIViewController {

    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var clearButton: UIButton!
    
    var loactionManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Put a Marker"
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        clearButton.layer.cornerRadius = clearButton.bounds.height / 2
        clearButton.layer.borderWidth = 0.3
        clearButton.layer.borderColor = UIColor.black.cgColor
        clearButton.backgroundColor = UIColor.init(crayola:"Maroon")
        clearButton.setTitleColor(UIColor.black, for: .normal)
        view.bringSubviewToFront(clearButton)
        loactionManager.startUpdatingLocation()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title:"History", style:.plain, target:self, action:#selector(showHistoryVC))
    }
    
    func createMarker(coordinate:CLLocation) -> GMSMarker {
        let marker = GMSMarker(position: CLLocationCoordinate2D(latitude:coordinate.coordinate.latitude, longitude:coordinate.coordinate.longitude))
        let serverData = ServerReqest()
        serverData.lat = coordinate.coordinate.latitude
        serverData.lon = coordinate.coordinate.longitude
        NetworkManager.shared.getWeather(data: serverData) { (ServerResponse) in
            print("Sucsses")
            guard let param = ServerResponse.wearher.first,
                  let feelTemp = ServerResponse.info?.feelsTemperature,
                  let pressure = ServerResponse.info?.pressure,
                  let humidity = ServerResponse.info?.humidity,
                  let windSpeed = ServerResponse.windParam?.windSpeed,
                  let windDir = ServerResponse.windParam?.windDirection,
                  let temp = ServerResponse.info?.temperature,
                  let place = ServerResponse.namePlace,
                  let country = ServerResponse.system?.country else {
                print("Something went wrong")
                return
            }
            let userInfo = InfoForUser()
            userInfo.sky = param.sky
            userInfo.skyState = param.skyDescription
            userInfo.temperature = temp
            userInfo.country = country
            userInfo.feelsTemperature = feelTemp
            userInfo.pressure = pressure
            userInfo.humidity = humidity
            userInfo.windSpeed = Int(windSpeed)
            userInfo.windDirection = windDir
            userInfo.lat = coordinate.coordinate.latitude
            userInfo.lon = coordinate.coordinate.longitude
            userInfo.place = place
            marker.title = "\(userInfo.temperature),\(userInfo.sky),\(place)"
            let date = NSDate()
            let calendar = NSCalendar.current
            let year = calendar.component(.year, from: date as Date)
            let day = calendar.component(.day , from:date as Date)
            let hour = calendar.component(.hour, from: date as Date)
            let minute = calendar.component(.minute, from:date as Date)
            userInfo.createDate = "\(year).\(day).\(hour).\(minute)"
            DataSource.shared.data.append(userInfo)
        } failure: { (error) in
            print(error)
        }
        return marker
    }
    
    func changeCamera(coordinate:CLLocation) {
        let camera = GMSCameraPosition(latitude:coordinate.coordinate.latitude, longitude:coordinate.coordinate.longitude, zoom: 25)
        mapView.camera = camera
        
    }
    
    
 @objc func showHistoryVC() {
        let tabBar = UITabBarController()
        var controllets = [UIViewController]()
        let historyVC = HistoryVC()
        let wikiVC = Wikipedia()
        historyVC.tabBarItem = UITabBarItem(title:"History", image: UIImage(systemName:"magnifyingglass"), tag: 0)
    wikiVC.tabBarItem = UITabBarItem(title:"Wikipedia", image:UIImage(systemName:"book" ), tag: 1)
    controllets.append(historyVC)
    controllets.append(wikiVC)
    tabBar.viewControllers = controllets
    
    navigationController?.pushViewController(tabBar, animated: true)
    
    
    
    }
    
    
    @IBAction func clearActrion(_ sender: Any) {
        mapView.clear()
    }
    

}

extension MapVC : GMSMapViewDelegate, CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let currentPosition = locations.last else {return}
        loactionManager.stopUpdatingLocation()
        changeCamera(coordinate:currentPosition)
        
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        createMarker(coordinate:CLLocation(latitude:coordinate.latitude, longitude:coordinate.longitude)).map = mapView
        print(coordinate)
    }
}

//
//  ViewController.swift
//  WeatherApp
//
//  Created by Raman Krutsiou on 5/7/21.
//

import UIKit
import EDColor

class ViewController: UIViewController {

    @IBOutlet weak var startButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = true
        startButton.layer.cornerRadius = startButton.bounds.height / 2
        startButton.layer.borderWidth = 2
        startButton.layer.borderColor = UIColor.black.cgColor
        startButton.backgroundColor = UIColor.init(crayola:"Green")
        startButton.setTitleColor(UIColor.black, for: .normal)
    }

    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
    }
    
    @IBAction func startAction(_ sender: Any) {
        guard let mapVC = getStoryboard().instantiateViewController(identifier:"mapVC") as? MapVC else { return }
        navigationController?.pushViewController(mapVC, animated:true)
    }
    
}


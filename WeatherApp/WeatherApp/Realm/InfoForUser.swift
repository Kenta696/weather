//
//  InfoForUser.swift
//  WeatherApp
//
//  Created by Raman Krutsiou on 5/8/21.
//

import Foundation


class InfoForUser  {
     var lat = 0.0
     var lon = 0.0
     var sky = ""
     var skyState = ""
     var temperature = 0
     var feelsTemperature  = 0.0
     var pressure = 0
     var humidity = 0
     var windSpeed = 0
     var windDirection = 0
     var country = ""
     var place = ""
     var createDate = ""
    
    
}
